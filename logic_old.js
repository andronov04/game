/*
var pinsFormation = [];
var pins = [ 6 ];
pinsFormation.push( pins );
pins = [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ];
pinsFormation.push( pins );
pins = [ 0 ];
pinsFormation.push( pins );
pins = []; // cut the rope ;)
pinsFormation.push( pins );
pins = [ 0, cloth.w ]; // classic 2 pins
pinsFormation.push( pins );
pins = pinsFormation[ 1 ];
function togglePins() {
    pins = pinsFormation[ ~~ ( Math.random() * pinsFormation.length ) ];
}*/
if ( WEBGL.isWebGLAvailable() === false ) {
    document.body.appendChild( WEBGL.getWebGLErrorMessage() );
}
var container, stats;
var camera, scene, renderer;
var object;

var clock = new THREE.Clock();
var time = 0;
//LINE SHADER
var lineVertShader = `
  attribute float lineDistance;
  varying float vLineDistance;
  
  void main() {
    vLineDistance = lineDistance;
    vec4 mvPosition = modelViewMatrix * vec4( position, 1 );
    gl_Position = projectionMatrix * mvPosition;
  }
  `;

var lineFragShader = `
  uniform vec3 diffuse;
  uniform float opacity;
  uniform float time; // added time uniform

  uniform float dashSize;
  uniform float gapSize;
  uniform float dotSize;
  varying float vLineDistance;
  
  void main() {
		float totalSize = dashSize + gapSize;
		float modulo = mod( vLineDistance + time, totalSize ); // time added to vLineDistance
    float dotDistance = dashSize + (gapSize * .5) - (dotSize * .5);
    
    if ( modulo > dashSize && mod(modulo, dotDistance) > dotSize ) {
      discard;
    }

    gl_FragColor = vec4( diffuse, opacity );
  }
  `;
var lineMat = new THREE.ShaderMaterial({
    uniforms: {
        diffuse: {value: new THREE.Color("white")},
        dashSize: {value: 15},
        gapSize: {value: 10},
        dotSize: {value: 2},
        opacity: {value: 1.0},
        time: {value: 0} // added uniform
    },
    linewidth: 2,
    vertexShader: lineVertShader,
    fragmentShader: lineFragShader,
    transparent: true
});

init();
animate();
function init() {
    container = document.createElement( 'div' );
    document.body.appendChild( container );
    // scene
    scene = new THREE.Scene();
    scene.background = new THREE.Color( 0xa0469 );
    scene.fog = new THREE.Fog( 0xa0469, 500, 10000 );
    // camera
    camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 10000 );
    camera.position.set( 1000, 0, 1500 );
    //camera = new THREE.PerspectiveCamera(60, window.innerWidth / window.innerHeight, 0.01, 1000);
    //camera.position.set(0, 0, 10);
    // lights
    scene.add( new THREE.AmbientLight( 0x666666 ) );
    var light = new THREE.DirectionalLight( 0xdfebff, 1 );
    light.position.set( 50, 200, 100 );
    light.position.multiplyScalar( 1.3 );
    light.castShadow = true;
    light.shadow.mapSize.width = 1024;
    light.shadow.mapSize.height = 1024;
    var d = 300;
    light.shadow.camera.left = - d;
    light.shadow.camera.right = d;
    light.shadow.camera.top = d;
    light.shadow.camera.bottom = 0;
    light.shadow.camera.far = 1000;
    scene.add( light );
    // cloth material
    var loader = new THREE.TextureLoader();

    // ground
    var groundTexture = loader.load( 'textures/terrain/grasslight-big.jpg' );
    groundTexture.wrapS = groundTexture.wrapT = THREE.RepeatWrapping;
    groundTexture.repeat.set( 25, 25 );
    groundTexture.anisotropy = 16;
    //var groundMaterial = new THREE.MeshLambertMaterial( { map: groundTexture } );// new THREE.MeshBasicMaterial({color: 0xa0469});//
    var groundMaterial = new THREE.MeshBasicMaterial({color: 0xa0469});
    var mesh = new THREE.Mesh( new THREE.PlaneBufferGeometry( 20000, 20000 ), groundMaterial );
    mesh.position.y = 0;
    mesh.rotation.x = - Math.PI / 2;
    mesh.receiveShadow = true;
    scene.add( mesh );

    // poles
    var poleGeo = new THREE.BoxBufferGeometry( 3, 103, 3 );
    var poleMat = new THREE.MeshLambertMaterial();
    var r_mesh = new THREE.Mesh( poleGeo, poleMat );
    r_mesh.position.x = - 75;
    r_mesh.position.y = 0;
    r_mesh.receiveShadow = true;
    r_mesh.castShadow = true;
    scene.add( r_mesh );
    var l_mesh = new THREE.Mesh( poleGeo, poleMat );
    l_mesh.position.x = 75;
    l_mesh.position.y = 0;
    l_mesh.receiveShadow = true;
    l_mesh.castShadow = true;
    scene.add( l_mesh );
    var u_mesh = new THREE.Mesh( new THREE.BoxBufferGeometry( 150, 3, 3 ), poleMat );
    u_mesh.position.y = 50;
    u_mesh.position.x = 0;
    u_mesh.receiveShadow = true;
    u_mesh.castShadow = true;
    scene.add( u_mesh );


    // LINE
    var subdivisions = 2;
    var recursion = 1;
    var points = [
        new THREE.Vector3( 225, 0, 344),
        new THREE.Vector3( 14, 0, 150),
        new THREE.Vector3( 50, 0, -20),
    ];//hilbert3D( new THREE.Vector3( 0, 0, 0 ), 25.0, recursion, 0, 1, 2, 3, 4, 5, 6, 7 );
    var spline = new THREE.CatmullRomCurve3( points );
    var samples = spline.getPoints( points.length * subdivisions );
    var geometrySpline = new THREE.BufferGeometry().setFromPoints( samples );


    //var line = new THREE.Line( geometrySpline, lineMat);
    var line = new THREE.Line( geometrySpline, new THREE.LineDashedMaterial( { color: 0xffffff, linewidth: 2, dashSize: 15, gapSize: 12 } ) );
    line.computeLineDistances();
    scene.add( line );

    // TEST LINE
    /*var points = [];
    for (let i = 0; i < 10; i++) {
        points.push(new THREE.Vector3(
            THREE.Math.randInt(225, 344),
            THREE.Math.randInt(14, 150),
            THREE.Math.randInt(10, 200)
        ))
    }*/
    /* var points = [
         new THREE.Vector3( 225, 0, 344),
         new THREE.Vector3( 14, 0, 150),
         new THREE.Vector3( 50, 0, -70),
     ];

     var lineDistances = [];
     var d = 0;
     for (let i = 0; i < points.length; i++) {
         if (i > 0) {
             d += points[i].distanceTo(points[i - 1]);
         }
         lineDistances[i] = d;
     }

     var lineGeom = new THREE.BufferGeometry().setFromPoints(points);
     lineGeom.addAttribute('lineDistance', new THREE.BufferAttribute(new Float32Array(lineDistances), 1));

     var line = new THREE.Line(lineGeom, lineMat);
     scene.add(line);*/

    // FIELD  rect
    var points2 = [
        new THREE.Vector3( -750, 0, 0),
        new THREE.Vector3( 750, 0, 0),
        new THREE.Vector3( 750, 0, 1755),
        new THREE.Vector3( -750, 0, 1755),
        new THREE.Vector3( -750, 0, 0),
    ];
    var geometrySpline2 = new THREE.BufferGeometry().setFromPoints( points2 );
    var rect = new THREE.Line( geometrySpline2, new THREE.LineBasicMaterial( { color: 0xffffff, linewidth: 3 } ) );
    rect.computeLineDistances();
    scene.add(rect);

    // FIELD  rect_big
    var rb = [
        new THREE.Vector3( -330, 0, 0),
        new THREE.Vector3( 330, 0, 0),
        new THREE.Vector3( 330, 0, 228),
        new THREE.Vector3( -330, 0, 228),
        new THREE.Vector3( -330, 0, 0),
    ];
    var rbs = new THREE.BufferGeometry().setFromPoints(rb);
    var rect_big = new THREE.Line(rbs, new THREE.LineBasicMaterial( { color: 0xffffff, linewidth: 3 } ) );
    rect_big.computeLineDistances();
    scene.add(rect_big);

    // FIELD  rect_small
    var rs = [
        new THREE.Vector3( -150, 0, 0),
        new THREE.Vector3( 150, 0, 0),
        new THREE.Vector3( 150, 0, 75),
        new THREE.Vector3( -150, 0, 75),
        new THREE.Vector3( -150, 0, 0),
    ];
    var rss = new THREE.BufferGeometry().setFromPoints(rs);
    var rect_small = new THREE.Line(rss, new THREE.LineBasicMaterial( { color: 0xffffff, linewidth: 3 } ) );
    rect_small.computeLineDistances();
    scene.add(rect_small);

    // FIELD  lcenter
    var lcenter = [
        new THREE.Vector3( -750, 0, 877),
        new THREE.Vector3( 750, 0, 877),
    ];
    var slcenter = new THREE.BufferGeometry().setFromPoints( lcenter );
    var rcenter = new THREE.Line( slcenter, new THREE.LineBasicMaterial( { color: 0xffffff, linewidth: 3 } ) );
    rcenter.computeLineDistances();
    scene.add(rcenter);

    // FIELD rect_s_circle
    // circle
    /*var segmentCount = 100,
        radius = 90,
        geometry = new THREE.Geometry(),
        material = new THREE.LineBasicMaterial({ color: 0xFFFFFF, linewidth: 3 });

    for (var i = 0; i <= segmentCount; i++) {
        var theta = (i / segmentCount) * Math.PI;
        geometry.vertices.push(
            new THREE.Vector3(
                Math.cos(theta) * radius,
                0,
                Math.sin(theta) * radius));
    }

    scene.add(new THREE.Line(geometry, material));*/
    /*var material = new THREE.LineBasicMaterial({ color: 0xFFFFFF, linewidth: 3 });

    var radius = 5;
    var segments = 32; //<-- Increase or decrease for more resolution I guess

    var circleGeometry = new THREE.CircleGeometry( radius, segments, 0, Math.PI * 1 );
    console.log(circleGeometry);
    var circle = new THREE.Mesh( circleGeometry, material );
    scene.add( circle );*/
    /* var resolution = 100;
     var amplitude = 100;
     var size = 360 / resolution;

     var geometry = new THREE.Geometry();
     var material = new THREE.LineBasicMaterial( { color: 0xFFFFFF, linewidth: 3} );
     for(var i = 0; i <= resolution; i++) {
         var segment = ( i * size ) * Math.PI / 180;
         geometry.vertices.push(  new THREE.Vector3( Math.cos( segment ) * amplitude, 0, Math.sin( segment ) * amplitude ) );
     }

     var line = new THREE.Line( geometry, material );
     scene.add(line);*/

    /* var positions = [];
     var colors = [];
     var points = hilbert3D( new THREE.Vector3( 0, 0, 0 ), 20.0, 1, 0, 1, 2, 3, 4, 5, 6, 7 );
     var spline = new THREE.CatmullRomCurve3( points );
     var divisions = Math.round( 12 * points.length );
     var color = new THREE.Color();
     for ( var i = 0, l = divisions; i < l; i ++ ) {
         var point = spline.getPoint( i / l );
         positions.push( point.x, point.y, point.z );
         color.setHSL( i / l, 1.0, 0.5 );
         colors.push( color.r, color.g, color.b );
     }
     // THREE.Line2 ( LineGeometry, LineMaterial )
     var geometry = new THREE.LineGeometry();
     geometry.setPositions( positions );
     geometry.setColors( colors );
     matLine = new THREE.LineMaterial( {
         color: 0xffffff,
         linewidth: 5, // in pixels
         vertexColors: THREE.VertexColors,
         //resolution:  // to be set by renderer, eventually
         dashed: false
     } );
     line = new THREE.Line2( geometry, matLine );
     line.computeLineDistances();
     line.scale.set( 1, 1, 1 );
     scene.add( line );
     // THREE.Line ( BufferGeometry, LineBasicMaterial ) - rendered with gl.LINE_STRIP
     var geo = new THREE.BufferGeometry();
     geo.addAttribute( 'position', new THREE.Float32BufferAttribute( positions, 3 ) );
     geo.addAttribute( 'color', new THREE.Float32BufferAttribute( colors, 3 ) );
     matLineBasic = new THREE.LineBasicMaterial( { vertexColors: THREE.VertexColors } );
     matLineDashed = new THREE.LineDashedMaterial( { vertexColors: THREE.VertexColors, scale: 2, dashSize: 1, gapSize: 1 } );
     line1 = new THREE.Line( geo, matLineBasic );
     line1.computeLineDistances();
     line1.visible = false;
     scene.add( line1 );*/

    /*var gg = new THREE.BoxBufferGeometry( 10, 10, 10 );
    var mesh = new THREE.Mesh( gg, poleMat );
    mesh.position.y = - 250;
    mesh.position.x = 125;
    mesh.receiveShadow = true;
    mesh.castShadow = true;
    scene.add( mesh );
    var mesh = new THREE.Mesh( gg, poleMat );
    mesh.position.y = - 250;
    mesh.position.x = - 125;
    mesh.receiveShadow = true;
    mesh.castShadow = true;
    scene.add( mesh );*/
    // renderer
    renderer = new THREE.WebGLRenderer( { antialias: true } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight );
    container.appendChild( renderer.domElement );
    renderer.gammaInput = true;
    renderer.gammaOutput = true;
    renderer.shadowMap.enabled = true;
    // controls
    var controls = new THREE.OrbitControls( camera, renderer.domElement );
    controls.maxPolarAngle = Math.PI * 0.5;
    controls.minDistance = 10;
    controls.maxDistance = 2000;
    controls.rotateSpeed = 0.07;
    // performance monitor
    stats = new Stats();
    container.appendChild( stats.dom );
    //
    window.addEventListener( 'resize', onWindowResize, false );
}
//
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
}
//
function animate() {
    requestAnimationFrame( animate );
    var time = Date.now();
    var windStrength = Math.cos( time / 7000 ) * 20 + 40;
    windForce.set( Math.sin( time / 2000 ), Math.cos( time / 3000 ), Math.sin( time / 1000 ) )
    windForce.normalize()
    windForce.multiplyScalar( windStrength );
    // simulate( time );
    render();
    stats.update();
}
function render() {
    time += clock.getDelta();
    lineMat.uniforms.time.value = time; // using of the time uniform

    renderer.render( scene, camera );
}


/*if ( WEBGL.isWebGLAvailable() === false ) {
    document.body.appendChild( WEBGL.getWebGLErrorMessage() );
}
var line, renderer, scene, camera, camera2, controls;
var line1;
var matLine, matLineBasic, matLineDashed;
var stats;
var gui;
// viewport
var insetWidth;
var insetHeight;
init();
animate();
function init() {
    renderer = new THREE.WebGLRenderer( { antialias: true } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setClearColor( 0x000000, 0.0 );
    renderer.setSize( window.innerWidth, window.innerHeight );
    document.body.appendChild( renderer.domElement );
    scene = new THREE.Scene();
    camera = new THREE.PerspectiveCamera( 40, window.innerWidth / window.innerHeight, 1, 1000 );
    camera.position.set( - 40, 0, 60 );
    camera2 = new THREE.PerspectiveCamera( 40, 1, 1, 1000 );
    camera2.position.copy( camera.position );
    controls = new THREE.OrbitControls( camera, renderer.domElement );
    controls.minDistance = 10;
    controls.maxDistance = 500;
    // Position and Color Data
    var positions = [];
    var colors = [];
    var points = hilbert3D( new THREE.Vector3( 0, 0, 0 ), 20.0, 1, 0, 1, 2, 3, 4, 5, 6, 7 );
    var spline = new THREE.CatmullRomCurve3( points );
    var divisions = Math.round( 12 * points.length );
    var color = new THREE.Color();
    for ( var i = 0, l = divisions; i < l; i ++ ) {
        var point = spline.getPoint( i / l );
        positions.push( point.x, point.y, point.z );
        color.setHSL( i / l, 1.0, 0.5 );
        colors.push( color.r, color.g, color.b );
    }
    // THREE.Line2 ( LineGeometry, LineMaterial )
    var geometry = new THREE.LineGeometry();
    geometry.setPositions( positions );
    geometry.setColors( colors );
    matLine = new THREE.LineMaterial( {
        color: 0xffffff,
        linewidth: 5, // in pixels
        vertexColors: THREE.VertexColors,
        //resolution:  // to be set by renderer, eventually
        dashed: false
    } );
    line = new THREE.Line2( geometry, matLine );
    line.computeLineDistances();
    line.scale.set( 1, 1, 1 );
    scene.add( line );
    // THREE.Line ( BufferGeometry, LineBasicMaterial ) - rendered with gl.LINE_STRIP
    var geo = new THREE.BufferGeometry();
    geo.addAttribute( 'position', new THREE.Float32BufferAttribute( positions, 3 ) );
    geo.addAttribute( 'color', new THREE.Float32BufferAttribute( colors, 3 ) );
    matLineBasic = new THREE.LineBasicMaterial( { vertexColors: THREE.VertexColors } );
    matLineDashed = new THREE.LineDashedMaterial( { vertexColors: THREE.VertexColors, scale: 2, dashSize: 1, gapSize: 1 } );
    line1 = new THREE.Line( geo, matLineBasic );
    line1.computeLineDistances();
    line1.visible = false;
    scene.add( line1 );
    //
    window.addEventListener( 'resize', onWindowResize, false );
    onWindowResize();
    stats = new Stats();
    document.body.appendChild( stats.dom );
    initGui();
}
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
    insetWidth = window.innerHeight / 4; // square
    insetHeight = window.innerHeight / 4;
    camera2.aspect = insetWidth / insetHeight;
    camera2.updateProjectionMatrix();
}
function animate() {
    requestAnimationFrame( animate );
    stats.update();
    // main scene
    renderer.setClearColor( 0x000000, 0 );
    renderer.setViewport( 0, 0, window.innerWidth, window.innerHeight );
    // renderer will set this eventually
    matLine.resolution.set( window.innerWidth, window.innerHeight ); // resolution of the viewport
    renderer.render( scene, camera );
    // inset scene
    renderer.setClearColor( 0x222222, 1 );
    renderer.clearDepth(); // important!
    renderer.setScissorTest( true );
    renderer.setScissor( 20, window.innerHeight - insetHeight - 20, insetWidth, insetHeight );
    renderer.setViewport( 20, window.innerHeight - insetHeight - 20, insetWidth, insetHeight );
    camera2.position.copy( camera.position );
    camera2.quaternion.copy( camera.quaternion );
    // renderer will set this eventually
    matLine.resolution.set( insetWidth, insetHeight ); // resolution of the inset viewport
    renderer.render( scene, camera2 );
    renderer.setScissorTest( false );
}
//
function initGui() {
    gui = new dat.GUI();
    var param = {
        'line type': 0,
        'width (px)': 5,
        'dashed': false,
        'dash scale': 1,
        'dash / gap': 1
    };
    gui.add( param, 'line type', { 'LineGeometry': 0, 'gl.LINE': 1 } ).onChange( function ( val ) {
        switch ( val ) {
            case '0':
                line.visible = true;
                line1.visible = false;
                break;
            case '1':
                line.visible = false;
                line1.visible = true;
                break;
        }
    } );
    gui.add( param, 'width (px)', 1, 10 ).onChange( function ( val ) {
        matLine.linewidth = val;
    } );
    gui.add( param, 'dashed' ).onChange( function ( val ) {
        matLine.dashed = val;
        // dashed is implemented as a defines -- not as a uniform. this could be changed.
        // ... or LineDashedMaterial could be implemented as a separate material
        // temporary hack - renderer should do this eventually
        if ( val ) matLine.defines.USE_DASH = ""; else delete matLine.defines.USE_DASH;
        matLine.needsUpdate = true;
        line1.material = val ? matLineDashed : matLineBasic;
    } );
    gui.add( param, 'dash scale', 0.5, 6, 0.1 ).onChange( function ( val ) {
        matLine.dashScale = val;
        matLineDashed.scale = val;
    } );
    gui.add( param, 'dash / gap', { '2 : 1': 0, '1 : 1': 1, '1 : 2': 2 } ).onChange( function ( val ) {
        switch ( val ) {
            case '0':
                matLine.dashSize = 2;
                matLine.gapSize = 1;
                matLineDashed.dashSize = 2;
                matLineDashed.gapSize = 1;
                break;
            case '1':
                matLine.dashSize = 1;
                matLine.gapSize = 1;
                matLineDashed.dashSize = 1;
                matLineDashed.gapSize = 1;
                break;
            case '2':
                matLine.dashSize = 1;
                matLine.gapSize = 2;
                matLineDashed.dashSize = 1;
                matLineDashed.gapSize = 2;
                break;
        }
    } );
}*/


/*
var camera, scene, renderer;
init();
animate();
function init() {
    camera = new THREE.PerspectiveCamera( 33, window.innerWidth / window.innerHeight, 0.1, 100 );
    camera.position.z = 10;
    scene = new THREE.Scene();
    scene.background = new THREE.Color( 0, 0, 0 );
    renderer = new THREE.SVGRenderer();
    renderer.setSize( window.innerWidth, window.innerHeight );
    document.body.appendChild( renderer.domElement );
    //
    var vertices = [];
    var divisions = 50;
    for ( var i = 0; i <= divisions; i ++ ) {
        var v = ( i / divisions ) * ( Math.PI * 2 );
        var x = Math.sin( v );
        var z = Math.cos( v );
        vertices.push( x, 0, z );
    }
    var geometry = new THREE.BufferGeometry();
    geometry.addAttribute( 'position', new THREE.Float32BufferAttribute( vertices, 3 ) );
    //
    // for ( var i = 1; i <= 3; i ++ ) {
    //     var material = new THREE.LineBasicMaterial( {
    //         color: Math.random() * 0xffffff,
    //         linewidth: 10
    //     } );
    //     var line = new THREE.Line( geometry, material );
    //     line.scale.setScalar( i / 3 );
    //     scene.add( line );
    // }
    var material = new THREE.LineDashedMaterial( {
        color: 'blue',
        linewidth: 1,
        dashSize: 10,
        gapSize: 10
    } );
    var line = new THREE.Line( geometry, material );
    line.scale.setScalar( 2 );
    scene.add( line );
    //
    window.addEventListener( 'resize', onWindowResize, false );
}
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
}
function animate() {
    var count = 0;
    var time = performance.now() / 1000;
    scene.traverse( function ( child ) {
        child.rotation.x = count + ( time / 3 );
        child.rotation.z = count + ( time / 4 );
        count ++;
    } );
    renderer.render( scene, camera );
    requestAnimationFrame( animate );
}*/


/*var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );

var renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

var geometry = new THREE.BoxGeometry( 5, 5, 1 );
var material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
var cube = new THREE.Mesh( geometry, material );
scene.add( cube );

camera.position.z = 5;

var animate = function () {
    requestAnimationFrame( animate );

    cube.rotation.x += 0.01;
    cube.rotation.y += 0.01;

    renderer.render( scene, camera );
};

animate();*//*
var renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

var camera = new THREE.PerspectiveCamera( 23, window.innerWidth / window.innerHeight, 1, 500 );
camera.position.set( 0, 0, 100 );
camera.lookAt( 0, 0, 0 );

var scene = new THREE.Scene();

var material = new THREE.LineBasicMaterial({
    color: 0x0000ff
});

var geometry = new THREE.Geometry();
geometry.vertices.push(
    new THREE.Vector3( -10, 0, 0 ),
    new THREE.Vector3( 0, 10, 0 ),
    new THREE.Vector3( 10, 0, 0 )
);

var line = new THREE.Line( geometry, material );
scene.add( line );
renderer.render( scene, camera );*/