//      _
//  ___| |__   __ _ _ __   ___  ___
// / __| '_ \ / _` | '_ \ / _ \/ __|
// \__ \ | | | (_| | |_) |  __/\__ \
// |___/_| |_|\__,_| .__/ \___||___/
//                 |_|
// for going live better recode as class/objects
var numSpheres = 3;
var angRand = [numSpheres];
var spread = 10;
var radius = 500/5;
var radiusControl = 20;
var xPos;
var yPos;
var offsetY;
var selectSpheres = [];
var selectxPosSpheres = [];
var selectyPosSpheres = [];
var selectLines = [];

container = document.createElement( 'div' );
document.body.appendChild( container );
// scene
scene = new THREE.Scene();
scene.background = new THREE.Color( 0xa0469 );
scene.fog = new THREE.Fog( 0xa0469, 500, 10000 );

//Create a WebGLRenderer and turn on shadows in the renderer
var renderer = new THREE.WebGLRenderer();
renderer.shadowMap.enabled = true;
renderer.shadowMap.type = THREE.PCFSoftShadowMap; // default THREE.PCFShadowMap

//Create a PointLight and turn on shadows for the light
var light = new THREE.PointLight( 0xffffff, 1, 100 );
light.position.set( 0, 10, 0 );
light.castShadow = true;            // default false
scene.add( light );
camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 10000 );
camera.position.set( 1000, 0, 1500 );

//Set up shadow properties for the light
light.shadow.mapSize.width = 512;  // default
light.shadow.mapSize.height = 512; // default
light.shadow.camera.near = 0.5;       // default
light.shadow.camera.far = 500      // default

//Create a sphere that cast shadows (but does not receive them)
var sphereGeometry = new THREE.SphereBufferGeometry( 5, 32, 32 );
var sphereMaterial = new THREE.MeshStandardMaterial( { color: 0xff0000 } );
var sphere = new THREE.Mesh( sphereGeometry, sphereMaterial );
sphere.castShadow = true; //default is false
sphere.receiveShadow = false; //default
scene.add( sphere );

//Create a plane that receives shadows (but does not cast them)
var planeGeometry = new THREE.PlaneBufferGeometry( 200, 200, 32, 32 );
var planeMaterial = new THREE.MeshStandardMaterial( { color: 0x00ff00 } )
var plane = new THREE.Mesh( planeGeometry, planeMaterial );
plane.receiveShadow = true;
scene.add( plane );

//Create a helper for the shadow camera (optional)
var helper = new THREE.CameraHelper( light.shadow.camera );
scene.add( helper );


animate();
function animate() {
    render();
    requestAnimationFrame(animate);
}
function render() {
    renderer.render(scene, camera);
}