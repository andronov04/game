import listStore from "./store/listStore";

export default function() {
	const ListStore = new listStore();

	return {
		ListStore,
	};
}