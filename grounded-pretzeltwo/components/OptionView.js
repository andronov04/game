import React, { Component } from 'react';
import { AppRegistry, View, Image, Text } from 'react-native';
import Button from 'react-native-button';


export default class OptionView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      'answers': [
        {
          'id': 1,
          'correct': false,
          'text': 'HAZARD',
        },
        {
          'id': 2,
          'correct': true,
          'text': 'RONALDO',
        },
        {
          'id': 3,
          'correct': false,
          'text': 'POGBA',
        },
        {
          'id': 4,
          'correct': false,
          'text': 'IZMAILOV',
        }
      ]
    }
  }

  _handlePress(index) {
    /*let items = this.state.tips;
    items[index].use = true;
    this.setState(prevState => ({tips: items}))*/
  }

  render() {
    
    let {answers} = this.state;

    console.log('this', this);

    return (
      // Try setting `justifyContent` to `center`.
      // Try setting `flexDirection` to `row`.
      <View style={{
        flexDirection: 'row',
        height: '100%',
        width: '100%',
        flexWrap: 'wrap',
        display: 'flex',
        alignItems: 'center',
        paddingRight: 15, paddingLeft: 15, 
        justifyContent: 'center',
        backgroundColor: 'black'}} >

          <View style={{flex: 1, display: 'flex', justifyContent: 'space-around', alignItems: 'center', 
          flexDirection: 'column', height: '100%', width: '100%'}}>
            {answers.slice(0,2).map((item, i)=> {
            return <Button onPress={(index) => this._handlePress(i)} 
                    style={{color: 'white', fontSize: 20, fontWeight: 'bold'}} >
                      {item.text}
                    </Button>
            })}
          </View>

          <View style={{flex: 1, display: 'flex', justifyContent: 'space-around', alignItems: 'center', 
          flexDirection: 'column', height: '100%', width: '100%'}}>
            {answers.slice(2,4).map((item, i)=> {
            return <Button onPress={(index) => this._handlePress(i)} 
                    style={{color: 'white', height: '50px', fontSize: 20, fontWeight: 'bold'}} >
                      {item.text}
                    </Button>
            })}
          </View>

        </View>
    );
  }
}
