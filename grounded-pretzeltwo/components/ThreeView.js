import React, { Component } from 'react';
import { AppRegistry, View, Image, Button, Text } from 'react-native';
import LevelHeaderView from './LevelHeaderView';
import LevelTwoHeaderView from './LevelTwoHeaderView';
import OptionView from './OptionView';

import Expo from 'expo';
import * as THREE from 'three';
import ExpoTHREE from 'expo-three';

const level = {
  is_verify: false,
};
var drawCount = 0;
var line;
var geometrySpline;

export default class ThreeView extends Component {
  _onGLContextCreate(gl) {
    const scene = new THREE.Scene();
    scene.background = new THREE.Color(0xa0469);
    const camera = new THREE.PerspectiveCamera(
      75,
      gl.drawingBufferWidth / gl.drawingBufferHeight,
      0.1,
      1000
    );
    const renderer = ExpoTHREE.createRenderer({ gl });
    renderer.setSize(gl.drawingBufferWidth, gl.drawingBufferHeight);

    /* const geometry = new THREE.SphereBufferGeometry(1, 36, 36);
        const material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
        const sphere = new THREE.Mesh(geometry, material);
        scene.add(sphere);*/
    var groundMaterial = new THREE.MeshBasicMaterial({ color: 0xa0469 });
    var mesh = new THREE.Mesh(
      new THREE.PlaneBufferGeometry(20000, 20000),
      groundMaterial
    );
    mesh.position.y = 0;
    mesh.rotation.x = -Math.PI / 2;
    mesh.receiveShadow = true;
    scene.add(mesh);

    // poles
    var poleGeo = new THREE.BoxBufferGeometry(3, 103, 3);
    var poleMat = new THREE.MeshBasicMaterial({ color: 0xffffff });
    var r_mesh = new THREE.Mesh(poleGeo, poleMat);
    r_mesh.position.x = -75;
    r_mesh.position.y = 0;
    r_mesh.receiveShadow = true;
    r_mesh.castShadow = true;
    scene.add(r_mesh);
    var l_mesh = new THREE.Mesh(poleGeo, poleMat);
    l_mesh.position.x = 75;
    l_mesh.position.y = 0;
    l_mesh.receiveShadow = true;
    l_mesh.castShadow = true;
    scene.add(l_mesh);geometrySpline
    var u_mesh = new THREE.Mesh(
      new THREE.BoxBufferGeometry(150, 3, 3),
      poleMat
    );
    u_mesh.position.y = 50;
    u_mesh.position.x = 0;
    u_mesh.receiveShadow = true;
    u_mesh.castShadow = true;
    scene.add(u_mesh);

    // FIELD  rect
    var points2 = [
      new THREE.Vector3(-750, 0, 0),
      new THREE.Vector3(750, 0, 0),
      new THREE.Vector3(750, 0, 1755),
      new THREE.Vector3(-750, 0, 1755),
      new THREE.Vector3(-750, 0, 0),
    ];
    var geometrySpline2 = new THREE.BufferGeometry().setFromPoints(points2);
    var rect = new THREE.Line(
      geometrySpline2,
      new THREE.LineBasicMaterial({ color: 0xffffff, linewidth: 3 })
    );
    rect.computeLineDistances();
    scene.add(rect);

    // FIELD  rect_big
    var rb = [
      new THREE.Vector3(-330, 0, 0),
      new THREE.Vector3(330, 0, 0),
      new THREE.Vector3(330, 0, 228),
      new THREE.Vector3(-330, 0, 228),
      new THREE.Vector3(-330, 0, 0),
    ];
    var rbs = new THREE.BufferGeometry().setFromPoints(rb);
    var rect_big = new THREE.Line(
      rbs,
      new THREE.LineBasicMaterial({ color: 0xffffff, linewidth: 3 })
    );
    rect_big.computeLineDistances();
    scene.add(rect_big);

    // FIELD  rect_small
    var rs = [
      new THREE.Vector3(-150, 0, 0),
      new THREE.Vector3(150, 0, 0),
      new THREE.Vector3(150, 0, 75),
      new THREE.Vector3(-150, 0, 75),
      new THREE.Vector3(-150, 0, 0),
    ];
    var rss = new THREE.BufferGeometry().setFromPoints(rs);
    var rect_small = new THREE.Line(
      rss,
      new THREE.LineBasicMaterial({ color: 0xffffff, linewidth: 3 })
    );
    rect_small.computeLineDistances();
    scene.add(rect_small);

    // FIELD  lcenter
    var lcenter = [
      new THREE.Vector3(-750, 0, 877),
      new THREE.Vector3(750, 0, 877),
    ];
    var slcenter = new THREE.BufferGeometry().setFromPoints(lcenter);
    var rcenter = new THREE.Line(
      slcenter,
      new THREE.LineBasicMaterial({ color: 0xffffff, linewidth: 3 })
    );
    rcenter.computeLineDistances();
    scene.add(rcenter);

    camera.position.z = 500;
    camera.position.x = 100;
    camera.position.y = 100;

    //line
    var curve = new THREE.QuadraticBezierCurve3(
        new THREE.Vector3( 225, 0, 344),
        new THREE.Vector3( 14, 0, 150),
        new THREE.Vector3( 50, 0, -20),
    );

    var samples = curve.getPoints( 50 );
    console.log('samples:', samples);

    geometrySpline = new THREE.BufferGeometry().setFromPoints( samples );


    line = new THREE.Line( geometrySpline, new THREE.LineDashedMaterial( { color: 0xffffff, linewidth: 5, dashSize: 15, gapSize: 12 } ) );
    line.computeLineDistances();
    scene.add( line );

    line.geometry.setDrawRange( 0, 0 );

    function _animLine() {
      if (drawCount <= 100) {
        line.geometry.setDrawRange(0, drawCount);
        drawCount += 0.2;
        //console.log('draw', drawCount, line)
      }
    }

    const render = () => {
      requestAnimationFrame(render);
      //sphere.rotation.x += 0.01;
      //sphere.rotation.y += 0.01;
      _animLine();
      renderer.render(scene, camera);
      gl.endFrameEXP();
    };
    render();
  }

  render() {
    return (
      // Try setting `justifyContent` to `center`.
      // Try setting `flexDirection` to `row`.
      <Expo.GLView
        style={{ flex: 1 }}
        onContextCreate={this._onGLContextCreate}
      />
    );
  }
}
