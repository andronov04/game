import React, { Component } from 'react';
import { AppRegistry, Button, FlatList, StyleSheet, Text, View } from 'react-native';

export default class FlatListBasics extends Component {
  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={[
            { key: 'CHAMPIONS LEAGUE' },
            { key: 'WORLD CUP' },
            { key: 'EUROPE LEAGUE' },
            { key: 'ENGLISH CHAMPIONSHIP' },
            { key: 'TOP 50 HISTORY GOALS $' },
            { key: 'COPA AMERICA' },
            { key: 'BRAZIL' },
            { key: 'TOP 10 FUNNY GOALS' },
          ]}
          renderItem={({ item }) => <Text 
          onPress={() => {
            this.props.navigation.navigate('Level', {
              itemId: 0,
              otherParam: 'anything you want here',
            });
          }}
          style={styles.item}>{item.key}</Text>}
        />
        <Button
          title="Go back"
          onPress={() => this.props.navigation.goBack()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 22,
    backgroundColor: '#1b38cd',
  },
  item: {
    padding: 10,
    fontSize: 22,
    height: 44,
    color: '#fff',
    fontWeight: 'bold',
    textAlign: 'center',
    fontFamily: 'Teko-Medium',
  },
});
