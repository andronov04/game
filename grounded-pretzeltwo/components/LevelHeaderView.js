import React, { Component } from 'react';
import { AppRegistry, View, Image, Text } from 'react-native';
import Button from 'react-native-button';



export default class LevelHeaderView extends Component {
  render() {
    return (
      // Try setting `justifyContent` to `center`.

      
      // Try setting `flexDirection` to `row`.
      <View style={{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
        height: '100%',
        backgroundColor: 'black'
      }}>
        <View style={{width: '45%', backgroundColor: 'black', paddingTop: 15, paddingLeft: 15}} >
          <Text
            style={{color: '#ffffff', fontWeight: 'bold',
            fontFamily: 'Teko-Medium', fontSize: 24, lineHeight: 28}}
            >{`WHO
SCORED
THE
GOAL?`}
          </Text>
        </View>
        <View style={{width: '10%', backgroundColor: 'black', paddingTop: 15}} >
          <Text style={{color: 'white', fontSize: 20, textAlign: 'center', fontWeight: 'bold'}}>0</Text>
        </View>
        
        <View style={{
          width: '45%', backgroundColor: 'black', 
          paddingTop: 15, paddingRight: 15,
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'flex-end',
        }} >
          <Text style={{color: 'white', textAlign: 'right', fontSize: 20, fontWeight: 'bold'}}>1/10</Text>
          <Button style={{color: 'white', 
          textAlign: 'right', fontSize: 20, fontWeight: 'bold', paddingLeft: 15}}>||</Button>
        </View>
      
      </View>
    );
  }
}
