import React, { Component } from 'react';
import { AppRegistry, View, Image, Text } from 'react-native';
import Button from 'react-native-button';


export default class LevelTwoHeaderView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      'tips': [
        {
          'id': 1,
          'use': false,
          'text': 'PLACE',
          'answer': 'LONDON'
        },
        {
          'id': 2,
          'use': false,
          'text': 'DATE',
          'answer': '10/11/2008'
        },
        {
          'id': 3,
          'use': false,
          'text': 'MINUTE',
          'answer': '90+'
        }
      ]
    }
  }

  _handlePress(index) {
    let items = this.state.tips;
    items[index].use = true;
    this.setState(prevState => ({tips: items}))
  }

  render() {
    
    let {tips} = this.state;

    return (
      // Try setting `justifyContent` to `center`.
      // Try setting `flexDirection` to `row`.
      <View style={{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
        height: '100%',
        backgroundColor: 'black'
      }}>
        <View style={{width: '45%', backgroundColor: 'black', paddingRight: 15, paddingLeft: 15}} >
          <Text
            style={{color: '#ffffff', paddingTop: 10, opacity: 0.8,
            fontFamily: 'Teko-Medium', fontSize: 12}}
            >{`CHAMPIONS LEAGUE 2017-18`}
          </Text>
        </View>
        
        <View style={{width: '55%', flex: 1,
        flexDirection: 'row',
        paddingRight: 15, paddingLeft: 15, paddingTop: 10, 
        justifyContent: 'space-between',
        backgroundColor: 'black'}} >
          {tips.map((item, i)=> {
            return <Button onPress={(index) => this._handlePress(i)} 
            style={{color: 'white', fontSize: 12, fontWeight: 'bold'}} >
              {item.use ? item.answer : item.text}
            </Button>
          })}
        </View>
      </View>
    );
  }
}
