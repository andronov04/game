import React, { Component } from 'react';
import { AppRegistry, View, Image, Button, Text } from 'react-native';
import LevelHeaderView from './LevelHeaderView';
import LevelTwoHeaderView from './LevelTwoHeaderView';
import OptionView from './OptionView';

const level = {
  'is_verify': false
};


export default class LevelView extends Component {
  render() {
    return (
      // Try setting `justifyContent` to `center`.
      // Try setting `flexDirection` to `row`.
      <View style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        width: '100%',
        backgroundColor: 'black'
      }}>
        <View style={{height: '20%', backgroundColor: 'black'}} ><LevelHeaderView/></View>
        <View style={{height: '10%', backgroundColor: 'black'}} ><LevelTwoHeaderView/></View>
        <View style={{height: '55%', backgroundColor: 'black'}} />

        <View style={{height: '15%', backgroundColor: 'black'}} >
          <OptionView />
        </View>

      </View>
    );
  }
}
