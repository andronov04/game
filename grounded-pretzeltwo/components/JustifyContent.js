import React, { Component } from 'react';
import { AppRegistry, View, Image, Button } from 'react-native';

export default class JustifyContentBasics extends Component {
  render() {
    return (
      // Try setting `justifyContent` to `center`.
      // Try setting `flexDirection` to `row`.
      <View style={{height: '100%', flex: 1, backgroundColor: '#152955'}}>
        <Image style={{width:'100%', height:'93%'}} source={require('../assets/ibra.gif')} />
        <Button
          title="Go back"
          onPress={() => this.props.navigation.goBack()}
        />
      </View>
    );
  }
}