import React from 'react';
import { Button, View, Text, StyleSheet } from 'react-native';
import { StackNavigator } from 'react-navigation'; // Version can be specified in package.json

import { Constants, Font } from 'expo';

import { Provider } from "mobx-react/native";
import config from "./configureStore";

// You can import from local files
import ListView from './components/ListView';
import LevelView from './components/LevelView';
import JustifyContentBasics from './components/JustifyContent';
import ThreeView from './components/ThreeView';

/*<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Home Screen</Text>
        <Button
          title="Go to Details"
          onPress={() => {
            this.props.navigation.navigate('Details', {
              itemId: 86,
              otherParam: 'anything you want here',
            });
          }}
        />
      </View>*/
class HomeScreen extends React.Component {
  componentDidMount() {
    Font.loadAsync({
      'Teko-Medium': require('./assets/Teko-Medium.ttf'),
    });
  }

  render() {

    return (
      <ThreeView></ThreeView>
    )

    return (
      <View style={styles.container}>
        {/*<JustifyContentBasics />*/}
        <Text
          style={{color: '#ffffff', paddingTop: 10, fontWeight: 'bold',
          fontFamily: 'Teko-Medium', fontSize: 40, lineHeight: 40}}
          onPress={() => {
            this.props.navigation.navigate('Menu', {
              itemId: 86,
              otherParam: 'anything you want here',
            });
          }}
        >{`WHO
SCORED
THE
GOAL?`}</Text>

      <Text
          style={{color: '#ffffff', fontWeight: 'bold', textAlign: 'center',
          fontFamily: 'Teko-Medium', fontSize: 40}}
          onPress={() => {
            this.props.navigation.navigate('Menu', {
              itemId: 86,
              otherParam: 'anything you want here',
            });
          }}
        >PLAY</Text>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
    backgroundColor: '#e42725',
    padding: 15,
    height: '100%'
  },
});

/*class DetailsScreen extends React.Component {
  render() {
    const { navigation } = this.props;
    const itemId = navigation.getParam('itemId', 'NO-ID');
    const otherParam = navigation.getParam('otherParam', 'some default value');

    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Details Screen</Text>
        <Text>itemId: {JSON.stringify(itemId)}</Text>
        <Text>otherParam: {JSON.stringify(otherParam)}</Text>
        <Button
          title="Go to Details... again"
          onPress={() =>
            this.props.navigation.push('Details', {
              itemId: Math.floor(Math.random() * 100),
            })
          }
        />
        <Button
          title="Go to Home"
          onPress={() => this.props.navigation.navigate('Home')}
        />
        <Button
          title="Go back"
          onPress={() => this.props.navigation.goBack()}
        />
      </View>
    );
  }
}*/

const RootStack = StackNavigator(
  {
    Home: {
      screen: HomeScreen,
    },
    Menu: {
      screen: ListView,
    },
    Level: {
      screen: JustifyContentBasics,
    },
  },
  {
    initialRouteName: 'Home',
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    }
  },
);

const stores  = config();

export default class App extends React.Component {
  render() {
    return <Provider {...stores}>
    <RootStack />
    </Provider>
    ;
  }
}
