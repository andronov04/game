/*
var pinsFormation = [];
var pins = [ 6 ];
pinsFormation.push( pins );
pins = [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ];
pinsFormation.push( pins );
pins = [ 0 ];
pinsFormation.push( pins );
pins = []; // cut the rope ;)
pinsFormation.push( pins );
pins = [ 0, cloth.w ]; // classic 2 pins
pinsFormation.push( pins );
pins = pinsFormation[ 1 ];
function togglePins() {
    pins = pinsFormation[ ~~ ( Math.random() * pinsFormation.length ) ];
}*/
if ( WEBGL.isWebGLAvailable() === false ) {
    document.body.appendChild( WEBGL.getWebGLErrorMessage() );
}
var container, stats;
var camera, scene, renderer;
var object;
var box;

//sphere
var sphere;

//line
var t = 0;
var drawCount = 0;
var drawCount2 = 0;
var drawCount3 = 0;
var counter = 0;
var line;
var curve;
var curve2;
var curve3;
var line2;
var line3;
var circlep;
var circlep2;
var circ2;
var geometrySpline;
var geometrySpline3;
var circleframe = 0;
var l_positions = [];
var positionXEnemy = -25;
var positionXEnemy2 = -55;

var clock = new THREE.Clock();
var time = 0;
//LINE SHADER
var lineVertShader = `
  attribute float lineDistance;
  varying float vLineDistance;
  
  void main() {
    vLineDistance = lineDistance;
    vec4 mvPosition = modelViewMatrix * vec4( position, 1 );
    gl_Position = projectionMatrix * mvPosition;
  }
  `;

var lineFragShader = `
  uniform vec3 diffuse;
  uniform float opacity;
  uniform float time; // added time uniform

  uniform float dashSize;
  uniform float gapSize;
  uniform float dotSize;
  varying float vLineDistance;
  
  void main() {
		float totalSize = dashSize + gapSize;
		float modulo = mod( vLineDistance + time, totalSize ); // time added to vLineDistance
    float dotDistance = dashSize + (gapSize * .5) - (dotSize * .5);
    
    if ( modulo > dashSize && mod(modulo, dotDistance) > dotSize ) {
      discard;
    }

    gl_FragColor = vec4( diffuse, opacity );
  }
  `;
var lineMat = new THREE.ShaderMaterial({
    uniforms: {
        diffuse: {value: new THREE.Color("white")},
        dashSize: {value: 10},
        gapSize: {value: 10},
        dotSize: {value: 2},
        opacity: {value: 1.0},
        time: {value: 0} // added uniform
    },
    linewidth: 2,
    vertexShader: lineVertShader,
    fragmentShader: lineFragShader,
    transparent: true
});

class User {

    constructor(name) {
        this.name = name;
    }

    sayHi() {
        alert(this.name);
    }

}

init();
animate();
function init() {
    container = document.createElement( 'div' );
    document.body.appendChild( container );
    // scene
    scene = new THREE.Scene();
    scene.background = new THREE.Color( 0xa0469 );
    scene.fog = new THREE.Fog( 0xa0469, 500, 10000 );
    // camera
    camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 10000 );
    camera.position.set( 1000, 0, 1500 );
    //camera = new THREE.PerspectiveCamera(60, window.innerWidth / window.innerHeight, 0.01, 1000);
    //camera.position.set(0, 0, 10);
    // lights
    scene.add( new THREE.AmbientLight( 0x666666 ) );
    var light = new THREE.DirectionalLight( 0xdfebff, 1 );
    light.position.set( 50, 200, 100 );
    light.position.multiplyScalar( 1.3 );
    light.castShadow = true;
    light.shadow.mapSize.width = 1024;
    light.shadow.mapSize.height = 1024;
    var d = 300;
    light.shadow.camera.left = - d;
    light.shadow.camera.right = d;
    light.shadow.camera.top = d;
    light.shadow.camera.bottom = 0;
    light.shadow.camera.far = 1000;
    scene.add( light );
    // cloth material
    var loader = new THREE.TextureLoader();

    // ground
    var groundTexture = loader.load( 'textures/terrain/grasslight-big.jpg' );
    groundTexture.wrapS = groundTexture.wrapT = THREE.RepeatWrapping;
    groundTexture.repeat.set( 25, 25 );
    groundTexture.anisotropy = 16;
    //var groundMaterial = new THREE.MeshLambertMaterial( { map: groundTexture } );// new THREE.MeshBasicMaterial({color: 0xa0469});//
    var groundMaterial = new THREE.MeshBasicMaterial({color: 0xa0469});
    var mesh = new THREE.Mesh( new THREE.PlaneBufferGeometry( 20000, 20000 ), groundMaterial );
    mesh.position.y = 0;
    mesh.rotation.x = - Math.PI / 2;
    mesh.receiveShadow = true;
    scene.add( mesh );

    // poles
    var poleGeo = new THREE.BoxBufferGeometry( 3, 103, 3 );
    //var poleMat = new THREE.MeshLambertMaterial();
    var poleMat = new THREE.MeshBasicMaterial({color: 0xffffff});
    var r_mesh = new THREE.Mesh( poleGeo, poleMat );
    r_mesh.position.x = - 75;
    r_mesh.position.y = 0;
    r_mesh.receiveShadow = true;
    r_mesh.castShadow = true;
    scene.add( r_mesh );
    var l_mesh = new THREE.Mesh( poleGeo, poleMat );
    l_mesh.position.x = 75;
    l_mesh.position.y = 0;
    l_mesh.receiveShadow = true;
    l_mesh.castShadow = true;
    scene.add( l_mesh );
    var u_mesh = new THREE.Mesh( new THREE.BoxBufferGeometry( 150, 3, 3 ), poleMat );
    u_mesh.position.y = 50;
    u_mesh.position.x = 0;
    u_mesh.receiveShadow = true;
    u_mesh.castShadow = true;
    scene.add( u_mesh );


    // LINE
   /* var subdivisions = 2;
    var recursion = 1;
    var points = [
        new THREE.Vector3( 225, 0, 344),
        new THREE.Vector3( 14, 0, 150),
        new THREE.Vector3( 50, 0, -20),
    ];
    var spline = new THREE.CatmullRomCurve3( points );
    var samples = spline.getPoints( points.length * subdivisions );*/


    curve = new THREE.QuadraticBezierCurve3(
        new THREE.Vector3( 450, 0, 344),
        new THREE.Vector3( 150, 67, 150),
        new THREE.Vector3( -35, 0, 120)
    );
    var samples = curve.getPoints( 100 );
    //console.log('samples:', samples);
    geometrySpline = new THREE.BufferGeometry().setFromPoints( samples );
    //line = new THREE.Line( geometrySpline, lineMat);
    line = new THREE.Line( geometrySpline, new THREE.LineDashedMaterial( { color: 0xffffff, linewidth: 1, dashSize: 6, gapSize: 6 } ) );
    line.computeLineDistances();
    scene.add( line );
    line.geometry.setDrawRange( 0, 0 );

    //console.log('curve:', curve);
  //  console.log('LINE:', line);

    // LINE TWO
    curve2 = new THREE.QuadraticBezierCurve3(
        new THREE.Vector3( 45, 0, 190),
        new THREE.Vector3( -10, 0, 100),
        new THREE.Vector3( 50, 20, -20),
    );
    var samples2 = curve2.getPoints( 100 );
   // console.log('samples:', samples2);
    geometrySpline2 = new THREE.BufferGeometry().setFromPoints( samples2 );
    //line = new THREE.Line( geometrySpline, lineMat);
    line2 = new THREE.Line( geometrySpline2, new THREE.LineDashedMaterial( { color: 0xffffff, linewidth: 1, dashSize: 6, gapSize: 6 } ) );
    line2.computeLineDistances();
    scene.add( line2 );
    line2.geometry.setDrawRange( 0, 0 );

    // LINE THREE (FULL)
    curve3 = new THREE.LineCurve3(
        new THREE.Vector3( -35, 0, 120),
        new THREE.Vector3( 45, 0, 190),
    );
    var samples3 = curve3.getPoints( 100 );
    // console.log('samples:', samples2);
    geometrySpline3 = new THREE.BufferGeometry().setFromPoints( samples3 );
    //line = new THREE.Line( geometrySpline, lineMat);
    line3 = new THREE.Line( geometrySpline3, new THREE.LineBasicMaterial( { color: 0xffffff, linewidth:1, dashSize: 6, gapSize: 6 } ) );
    line3.computeLineDistances();
    scene.add( line3 );
    line3.geometry.setDrawRange( 0, 0 );

    // LINE THREE (FULL) opacity
    /*var curve33 = new THREE.LineCurve3(
        new THREE.Vector3( -35, 0, 120),
        new THREE.Vector3( 45, 0, 190),
    );
    var samples33 = curve33.getPoints( 100 );
    // console.log('samples:', samples2);
    var geometrySpline33 = new THREE.BufferGeometry().setFromPoints( samples33 );
    //line = new THREE.Line( geometrySpline, lineMat);
    var line33 = new THREE.Line( geometrySpline33, new THREE.LineBasicMaterial( { color: 0xffffff, linewidth:1, dashSize: 6, gapSize: 6 } ) );
    line33.computeLineDistances();
    scene.add( line33 );*/

    // PLAYER ENEMY
    var ecurve1 = new THREE.LineCurve3(
        new THREE.Vector3( 100, 0, 100),
        new THREE.Vector3( 110, 0, 110),
    );
    var esamples1 = ecurve1.getPoints( 100 );
    // console.log('samples:', samples2);
    var egeometrySpline1 = new THREE.BufferGeometry().setFromPoints( esamples1 );
    //line = new THREE.Line( geometrySpline, lineMat);
    var eline1 = new THREE.Line( egeometrySpline1, new THREE.LineBasicMaterial( { color: 0xffffff, linewidth:2} ) );
    eline1.computeLineDistances();
    scene.add(eline1 );

    var ecurve2 = new THREE.LineCurve3(
        new THREE.Vector3( 100, 0, 110),
        new THREE.Vector3( 110, 0, 100),
    );
    var esamples2 = ecurve2.getPoints( 100 );
    // console.log('samples:', samples2);
    var egeometrySpline2 = new THREE.BufferGeometry().setFromPoints( esamples2 );
    //line = new THREE.Line( geometrySpline, lineMat);
    var eline2 = new THREE.Line( egeometrySpline2, new THREE.LineBasicMaterial( { color: 0xffffff, linewidth:2} ) );
    eline2.computeLineDistances();
    scene.add(eline2 );


    // PLAYER ENEMY 2
    var ecurve1 = new THREE.LineCurve3(
        new THREE.Vector3( -150, 0, 150),
        new THREE.Vector3( -160, 0, 160),
    );
    var esamples1 = ecurve1.getPoints( 100 );
    // console.log('samples:', samples2);
    var egeometrySpline1 = new THREE.BufferGeometry().setFromPoints( esamples1 );
    //line = new THREE.Line( geometrySpline, lineMat);
    var eline1 = new THREE.Line( egeometrySpline1, new THREE.LineBasicMaterial( { color: 0xffffff, linewidth:2} ) );
    eline1.computeLineDistances();
    scene.add(eline1 );

    var ecurve2 = new THREE.LineCurve3(
        new THREE.Vector3( -150, 0, 160),
        new THREE.Vector3( -160, 0, 150),
    );
    var esamples2 = ecurve2.getPoints( 100 );
    // console.log('samples:', samples2);
    var egeometrySpline2 = new THREE.BufferGeometry().setFromPoints( esamples2 );
    //line = new THREE.Line( geometrySpline, lineMat);
    var eline2 = new THREE.Line( egeometrySpline2, new THREE.LineBasicMaterial( { color: 0xffffff, linewidth:2} ) );
    eline2.computeLineDistances();
    scene.add(eline2 );


    // PLAYER ENEMY 3
    var ecurve1 = new THREE.LineCurve3(
        new THREE.Vector3( 50, 0, 200),
        new THREE.Vector3( 60, 0, 210),
    );
    var esamples1 = ecurve1.getPoints( 100 );
    // console.log('samples:', samples2);
    var egeometrySpline1 = new THREE.BufferGeometry().setFromPoints( esamples1 );
    //line = new THREE.Line( geometrySpline, lineMat);
    var eline1 = new THREE.Line( egeometrySpline1, new THREE.LineBasicMaterial( { color: 0xffffff, linewidth:2} ) );
    eline1.computeLineDistances();
    scene.add(eline1 );

    var ecurve2 = new THREE.LineCurve3(
        new THREE.Vector3( 60, 0, 200),
        new THREE.Vector3( 50, 0, 210),
    );
    var esamples2 = ecurve2.getPoints( 100 );
    // console.log('samples:', samples2);
    var egeometrySpline2 = new THREE.BufferGeometry().setFromPoints( esamples2 );
    //line = new THREE.Line( geometrySpline, lineMat);
    var eline2 = new THREE.Line( egeometrySpline2, new THREE.LineBasicMaterial( { color: 0xffffff, linewidth:2} ) );
    eline2.computeLineDistances();
    scene.add(eline2 );

    //eline2.position.set(new THREE.Vector3( 25, 0, 120));



    // LINE FOUR WITH PLAYER
 /*   var curve4 = new THREE.LineCurve3(
        new THREE.Vector3( -35, 0, 120),
        new THREE.Vector3( -85, 0, 220),
    );
    var samples4 = curve4.getPoints( 100 );
    // console.log('samples:', samples2);
    var geometrySpline4 = new THREE.BufferGeometry().setFromPoints( samples4 );
    //line = new THREE.Line( geometrySpline, lineMat);
    var line4 = new THREE.Line( geometrySpline4, new THREE.LineDashedMaterial( { color: 0xffffff, linewidth:1, dashSize: 3, gapSize: 2 } ) );
    line4.computeLineDistances();
    scene.add( line4 );
    //line4.geometry.setDrawRange( 0, 0 );

    // LINE FOUR-FIVE WITH PLAYER
    var curve5 = new THREE.LineCurve3(
        new THREE.Vector3( -37, 0, 120),
        new THREE.Vector3( -87, 0, 220),
    );
    var samples5 = curve5.getPoints( 100 );
    // console.log('samples:', samples2);
    var geometrySpline5 = new THREE.BufferGeometry().setFromPoints( samples5 );
    //line = new THREE.Line( geometrySpline, lineMat);
    var line5 = new THREE.Line( geometrySpline5, new THREE.LineBasicMaterial( { color: 0xffffff, linewidth:1, dashSize: 6, gapSize: 6 } ) );
    line5.computeLineDistances();
    scene.add( line5 );
    //line4.geometry.setDrawRange( 0, 0 );*/


    // BALL
    var geometry = new THREE.SphereGeometry( 5, 32, 32 );
    var material = new THREE.MeshBasicMaterial( {color: 0xffffff} );
    sphere = new THREE.Mesh( geometry, material );
    scene.add( sphere );

    //console.log('SPHERE: ', sphere);


    // PLAYER CENTER
    var geometryp = new THREE.CircleGeometry( 3, 180 );
    var materialp = new THREE.MeshBasicMaterial( { color: 0xffffff } );
    circlep = new THREE.Mesh( geometryp, materialp );
    scene.add( circlep );

    circlep.position.set(45, 0, 190);
    circlep.rotateX(-Math.PI/2);
    circlep.scale.set(1.5, 1.5, 1.5);
    //circlep.geometry.parameters.radius = 20;

    // PLAYER
   // var dashMaterial = new THREE.LineDashedMaterial( { color: 0xffffff, dashSize: 2*Math.PI*10/40, gapSize: 2*Math.PI*10/40, linewidth:2  } ),
    var dashMaterial = new THREE.LineBasicMaterial( { color: 0xffffff, linewidth:1  } ),
        circGeom = new THREE.CircleGeometry( 5, 180, 0 );

    circGeom.vertices.shift();

    var circ = new THREE.Line( circGeom, dashMaterial);
    circ.computeLineDistances();
    scene.add( circ );

    circ.position.set(45, 0, 190);
    circ.rotateX(Math.PI/2);


    ///COPYCOPYCOPYCOPYCOPYCOPY
    // PLAYER CENTER
    var geometryp2 = new THREE.CircleGeometry( 3, 180 );
    var materialp2 = new THREE.MeshBasicMaterial( { color: 0xffffff } );
    circlep2 = new THREE.Mesh( geometryp2, materialp2 );
    scene.add( circlep2 );

    circlep2.position.set(-35, 0.3, 120);
    circlep2.rotateX(-Math.PI/2);
    circlep2.scale.set(1.5, 1.5, 1.5);
    //circlep.geometry.parameters.radius = 20;

    // PLAYER
    // var dashMaterial = new THREE.LineDashedMaterial( { color: 0xffffff, dashSize: 2*Math.PI*10/40, gapSize: 2*Math.PI*10/40, linewidth:2  } ),
    var dashMaterial = new THREE.LineBasicMaterial( { color: 0xffffff, linewidth:1  } ),
        circGeom = new THREE.CircleGeometry( 5, 180, 0 );

    circGeom.vertices.shift();

    circ2 = new THREE.Line( circGeom, dashMaterial);
    circ2.computeLineDistances();
    scene.add( circ2 );

    circ2.position.set(-35, 0, 120);
    circ2.rotateX(Math.PI/2);
    ///ENDENDENDENDENDENDENDEND


    //ENEMY PLAYER
   /* var egeometrySpline = new THREE.Geometry();
    egeometrySpline.vertices = [
        new THREE.Vector3(30,10,0),
        new THREE.Vector3(10,10,0),
        new THREE.Vector3(10,30,0)
    ];
    egeometrySpline.faces = [new THREE.Face3(1,0,2)];
    //line = new THREE.Line( geometrySpline, lineMat);
    var eline = new THREE.Mesh( egeometrySpline, new THREE.MeshBasicMaterial({ color: 0xffff00 }));
   // var eline = new THREE.Line( egeometrySpline, new THREE.LineBasicMaterial( { color: 0xffffff, linewidth: 2, dashSize: 12, gapSize: 12 } ) );
    scene.add( eline );*/

   // TEST LINE
    /*var points = [];
    for (let i = 0; i < 10; i++) {
        points.push(new THREE.Vector3(
            THREE.Math.randInt(225, 344),
            THREE.Math.randInt(14, 150),
            THREE.Math.randInt(10, 200)
        ))
    }*/
   /* var points = [
        new THREE.Vector3( 225, 0, 344),
        new THREE.Vector3( 14, 0, 150),
        new THREE.Vector3( 50, 0, -70),
    ];

    var lineDistances = [];
    var d = 0;
    for (let i = 0; i < points.length; i++) {
        if (i > 0) {
            d += points[i].distanceTo(points[i - 1]);
        }
        lineDistances[i] = d;
    }

    var lineGeom = new THREE.BufferGeometry().setFromPoints(points);
    lineGeom.addAttribute('lineDistance', new THREE.BufferAttribute(new Float32Array(lineDistances), 1));

    var line = new THREE.Line(lineGeom, lineMat);
    scene.add(line);*/

    // FIELD  rect
    var points2 = [
        new THREE.Vector3( -750, 0, 0),
        new THREE.Vector3( 750, 0, 0),
        new THREE.Vector3( 750, 0, 1755),
        new THREE.Vector3( -750, 0, 1755),
        new THREE.Vector3( -750, 0, 0),
    ];
    var geometrySpline2 = new THREE.BufferGeometry().setFromPoints( points2 );
    var rect = new THREE.Line( geometrySpline2, new THREE.LineBasicMaterial( { color: 0xffffff, linewidth: 3 } ) );
   // rect.computeLineDistances();
    scene.add(rect);

    // FIELD  rect_big
    var rb = [
        new THREE.Vector3( -330, 0, 0),
        new THREE.Vector3( 330, 0, 0),
        new THREE.Vector3( 330, 0, 228),
        new THREE.Vector3( -330, 0, 228),
        new THREE.Vector3( -330, 0, 0),
    ];
    var rbs = new THREE.BufferGeometry().setFromPoints(rb);
    var rect_big = new THREE.Line(rbs, new THREE.LineBasicMaterial( { color: 0xffffff, linewidth: 3 } ) );
  //  rect_big.computeLineDistances();
    scene.add(rect_big);

    // FIELD  rect_small
    var rs = [
        new THREE.Vector3( -150, 0, 0),
        new THREE.Vector3( 150, 0, 0),
        new THREE.Vector3( 150, 0, 75),
        new THREE.Vector3( -150, 0, 75),
        new THREE.Vector3( -150, 0, 0),
    ];
    var rss = new THREE.BufferGeometry().setFromPoints(rs);
    var rect_small = new THREE.Line(rss, new THREE.LineBasicMaterial( { color: 0xffffff, linewidth: 3 } ) );
    rect_small.computeLineDistances();
    scene.add(rect_small);

    // FIELD  lcenter
    var lcenter = [
        new THREE.Vector3( -750, 0, 877),
        new THREE.Vector3( 750, 0, 877),
    ];
    var slcenter = new THREE.BufferGeometry().setFromPoints( lcenter );
    var rcenter = new THREE.Line(slcenter, new THREE.LineBasicMaterial( { color: 0xffffff, linewidth: 3 } ));
    rcenter.computeLineDistances();
    scene.add(rcenter);

    // FIELD rect_s_circle
    // circle
    /*var segmentCount = 100,
        radius = 90,
        geometry = new THREE.Geometry(),
        material = new THREE.LineBasicMaterial({ color: 0xFFFFFF, linewidth: 3 });

    for (var i = 0; i <= segmentCount; i++) {
        var theta = (i / segmentCount) * Math.PI;
        geometry.vertices.push(
            new THREE.Vector3(
                Math.cos(theta) * radius,
                0,
                Math.sin(theta) * radius));
    }

    scene.add(new THREE.Line(geometry, material));*/
    /*var material = new THREE.LineBasicMaterial({ color: 0xFFFFFF, linewidth: 3 });

    var radius = 5;
    var segments = 32; //<-- Increase or decrease for more resolution I guess

    var circleGeometry = new THREE.CircleGeometry( radius, segments, 0, Math.PI * 1 );
    console.log(circleGeometry);
    var circle = new THREE.Mesh( circleGeometry, material );
    scene.add( circle );*/
   /* var resolution = 100;
    var amplitude = 100;
    var size = 360 / resolution;

    var geometry = new THREE.Geometry();
    var material = new THREE.LineBasicMaterial( { color: 0xFFFFFF, linewidth: 3} );
    for(var i = 0; i <= resolution; i++) {
        var segment = ( i * size ) * Math.PI / 180;
        geometry.vertices.push(  new THREE.Vector3( Math.cos( segment ) * amplitude, 0, Math.sin( segment ) * amplitude ) );
    }

    var line = new THREE.Line( geometry, material );
    scene.add(line);*/

   /* var positions = [];
    var colors = [];
    var points = hilbert3D( new THREE.Vector3( 0, 0, 0 ), 20.0, 1, 0, 1, 2, 3, 4, 5, 6, 7 );
    var spline = new THREE.CatmullRomCurve3( points );
    var divisions = Math.round( 12 * points.length );
    var color = new THREE.Color();
    for ( var i = 0, l = divisions; i < l; i ++ ) {
        var point = spline.getPoint( i / l );
        positions.push( point.x, point.y, point.z );
        color.setHSL( i / l, 1.0, 0.5 );
        colors.push( color.r, color.g, color.b );
    }
    // THREE.Line2 ( LineGeometry, LineMaterial )
    var geometry = new THREE.LineGeometry();
    geometry.setPositions( positions );
    geometry.setColors( colors );
    matLine = new THREE.LineMaterial( {
        color: 0xffffff,
        linewidth: 5, // in pixels
        vertexColors: THREE.VertexColors,
        //resolution:  // to be set by renderer, eventually
        dashed: false
    } );
    line = new THREE.Line2( geometry, matLine );
    line.computeLineDistances();
    line.scale.set( 1, 1, 1 );
    scene.add( line );
    // THREE.Line ( BufferGeometry, LineBasicMaterial ) - rendered with gl.LINE_STRIP
    var geo = new THREE.BufferGeometry();
    geo.addAttribute( 'position', new THREE.Float32BufferAttribute( positions, 3 ) );
    geo.addAttribute( 'color', new THREE.Float32BufferAttribute( colors, 3 ) );
    matLineBasic = new THREE.LineBasicMaterial( { vertexColors: THREE.VertexColors } );
    matLineDashed = new THREE.LineDashedMaterial( { vertexColors: THREE.VertexColors, scale: 2, dashSize: 1, gapSize: 1 } );
    line1 = new THREE.Line( geo, matLineBasic );
    line1.computeLineDistances();
    line1.visible = false;
    scene.add( line1 );*/

    /*var gg = new THREE.BoxBufferGeometry( 10, 10, 10 );
    var mesh = new THREE.Mesh( gg, poleMat );
    mesh.position.y = - 250;
    mesh.position.x = 125;
    mesh.receiveShadow = true;
    mesh.castShadow = true;
    scene.add( mesh );
    var mesh = new THREE.Mesh( gg, poleMat );
    mesh.position.y = - 250;
    mesh.position.x = - 125;
    mesh.receiveShadow = true;
    mesh.castShadow = true;
    scene.add( mesh );*/
    box = new THREE.Mesh(new THREE.BoxGeometry(2, 2, 2), new THREE.MeshBasicMaterial({
        color: "aqua"
    }));
    scene.add(box);

    // renderer
    renderer = new THREE.WebGLRenderer( { antialias: true } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight );
    container.appendChild( renderer.domElement );
    renderer.gammaInput = true;
    renderer.gammaOutput = true;
    renderer.shadowMap.enabled = true;
    // controls
    var controls = new THREE.OrbitControls( camera, renderer.domElement );
    controls.maxPolarAngle = Math.PI * 0.9;
    controls.minDistance = 10;
    controls.maxDistance = 1800;
    controls.rotateSpeed = 0.07;
    // performance monitor
    stats = new Stats();
    container.appendChild( stats.dom );
    //
    window.addEventListener( 'resize', onWindowResize, false );
}
//
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
}
//
function animate() {
    requestAnimationFrame( animate );
    var time = Date.now();
    var windStrength = Math.cos( time / 7000 ) * 20 + 40;
    windForce.set( Math.sin( time / 2000 ), Math.cos( time / 3000 ), Math.sin( time / 1000 ) )
    windForce.normalize()
    windForce.multiplyScalar( windStrength );
   // simulate( time )
    render();

    animLine()
    stats.update();
}

function animLine() {
    if (drawCount <= 100){
        // line.geometry.verticesNeedUpdate = true;
        // line.geometry.elementsNeedUpdate = true;
        // line.geometry.morphTargetsNeedUpdate = true;
        // line.geometry.uvsNeedUpdate = true;
        // line.geometry.normalsNeedUpdate = true;
        // line.geometry.colorsNeedUpdate = true;
        // line.geometry.tangentsNeedUpdate = true;
        curve.needsUpdate = true;
        line.geometry.setDrawRange( 0, drawCount );
        drawCount += 0.4;
        var pos = curve.getPointAt(counter);
        if(!isNaN(pos.x)){
            var rad = sphere.geometry.parameters.radius;
            pos.y = pos.y > rad ? pos.y : rad;
            sphere.position.set(pos.x, pos.y, pos.z);
        }
        //console.log('draw', drawCount, counter, pos);
        counter += 0.004
    }
    if (drawCount >= 100 && drawCount3 <= 100){
        if(drawCount3 === 0)counter = 0;
        line3.geometry.dynamic = true;
        line3.geometry.attributes.position.needsUpdate = true;
        line3.matrixAutoUpdate = true;
        line3.geometry.setDrawRange( 0, drawCount3 );
        drawCount3 += 0.6;
        // sphere.position.set(line.geometry.boundingSphere.center.x, line.geometry.boundingSphere.center.y, 0);
        //console.log('draw', drawCount, line)
        var pos = curve3.getPointAt(counter);
        if(!isNaN(pos.x)){
            var rad = sphere.geometry.parameters.radius;
            pos.y = pos.y > rad ? pos.y : rad;
            sphere.position.set(pos.x, pos.y, pos.z);
        }
        //console.log('draw', drawCount, counter, pos);
        counter += 0.006
    }
    if (drawCount3 >= 100 && drawCount2 <= 100){
        if(drawCount2 === 0)counter = 0;
        line2.geometry.dynamic = true;
        line2.geometry.attributes.position.needsUpdate = true;
        line2.matrixAutoUpdate = true;
        line2.geometry.setDrawRange( 0, drawCount2 );
        drawCount2 += 0.6;
        // sphere.position.set(line.geometry.boundingSphere.center.x, line.geometry.boundingSphere.center.y, 0);
        //console.log('draw', drawCount, line)
        var pos = curve2.getPointAt(counter);
        if(!isNaN(pos.x)){
            var rad = sphere.geometry.parameters.radius;
            pos.y = pos.y > rad ? pos.y : rad;
            sphere.position.set(pos.x, pos.y, pos.z);
        }
        //console.log('draw', drawCount, counter, pos);
        counter += 0.006
    }

    //Circle
    var mc = Math.sin(circleframe);
    circlep.scale.set(mc, mc, 0);
    //circleframe += 0.05;
    //copy
    circlep2.scale.set(mc, mc, 0);
    circleframe += 0.05;

   /* if(circ2.position.x <= positionXEnemy){
        positionXEnemy2 += 0.15;
        circ2.position.set(positionXEnemy2, 0, 120);
        circlep2.position.set(positionXEnemy2, 0, 120);
    }*/

}

/*window.addEventListener("click", onClick, false);

var forth = true;

function onClick() {

    new TWEEN.Tween(box.position)
        .to(box.position.clone().setX(forth ? 50 : 0), 1000)
        .onStart(function() {
        })
        .onComplete(function() {
            forth = !forth;
        })
        .start();

}*/
// var position = {x: -15, y: 0, z: 0};
// var target = {x: 4, y: 0, z: -15};
// var tween = new TWEEN.Tween(position).to(target, 8000);
// tween.easing(TWEEN.Easing.Elastic.InOut);
//
// tween.onUpdate(function() {
//     console.log('geometrySpline', line.geometry.);
//     geometrySpline.vertices.push(position.x, position.y, position.z);
//
// });
// tween.start();

function render() {
    //time += clock.getDelta();
   // lineMat.uniforms.time.value = time; // using of the time uniform


    renderer.render( scene, camera );
}


/*if ( WEBGL.isWebGLAvailable() === false ) {
    document.body.appendChild( WEBGL.getWebGLErrorMessage() );
}
var line, renderer, scene, camera, camera2, controls;
var line1;
var matLine, matLineBasic, matLineDashed;
var stats;
var gui;
// viewport
var insetWidth;
var insetHeight;
init();
animate();
function init() {
    renderer = new THREE.WebGLRenderer( { antialias: true } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setClearColor( 0x000000, 0.0 );
    renderer.setSize( window.innerWidth, window.innerHeight );
    document.body.appendChild( renderer.domElement );
    scene = new THREE.Scene();
    camera = new THREE.PerspectiveCamera( 40, window.innerWidth / window.innerHeight, 1, 1000 );
    camera.position.set( - 40, 0, 60 );
    camera2 = new THREE.PerspectiveCamera( 40, 1, 1, 1000 );
    camera2.position.copy( camera.position );
    controls = new THREE.OrbitControls( camera, renderer.domElement );
    controls.minDistance = 10;
    controls.maxDistance = 500;
    // Position and Color Data
    var positions = [];
    var colors = [];
    var points = hilbert3D( new THREE.Vector3( 0, 0, 0 ), 20.0, 1, 0, 1, 2, 3, 4, 5, 6, 7 );
    var spline = new THREE.CatmullRomCurve3( points );
    var divisions = Math.round( 12 * points.length );
    var color = new THREE.Color();
    for ( var i = 0, l = divisions; i < l; i ++ ) {
        var point = spline.getPoint( i / l );
        positions.push( point.x, point.y, point.z );
        color.setHSL( i / l, 1.0, 0.5 );
        colors.push( color.r, color.g, color.b );
    }
    // THREE.Line2 ( LineGeometry, LineMaterial )
    var geometry = new THREE.LineGeometry();
    geometry.setPositions( positions );
    geometry.setColors( colors );
    matLine = new THREE.LineMaterial( {
        color: 0xffffff,
        linewidth: 5, // in pixels
        vertexColors: THREE.VertexColors,
        //resolution:  // to be set by renderer, eventually
        dashed: false
    } );
    line = new THREE.Line2( geometry, matLine );
    line.computeLineDistances();
    line.scale.set( 1, 1, 1 );
    scene.add( line );
    // THREE.Line ( BufferGeometry, LineBasicMaterial ) - rendered with gl.LINE_STRIP
    var geo = new THREE.BufferGeometry();
    geo.addAttribute( 'position', new THREE.Float32BufferAttribute( positions, 3 ) );
    geo.addAttribute( 'color', new THREE.Float32BufferAttribute( colors, 3 ) );
    matLineBasic = new THREE.LineBasicMaterial( { vertexColors: THREE.VertexColors } );
    matLineDashed = new THREE.LineDashedMaterial( { vertexColors: THREE.VertexColors, scale: 2, dashSize: 1, gapSize: 1 } );
    line1 = new THREE.Line( geo, matLineBasic );
    line1.computeLineDistances();
    line1.visible = false;
    scene.add( line1 );
    //
    window.addEventListener( 'resize', onWindowResize, false );
    onWindowResize();
    stats = new Stats();
    document.body.appendChild( stats.dom );
    initGui();
}
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
    insetWidth = window.innerHeight / 4; // square
    insetHeight = window.innerHeight / 4;
    camera2.aspect = insetWidth / insetHeight;
    camera2.updateProjectionMatrix();
}
function animate() {
    requestAnimationFrame( animate );
    stats.update();
    // main scene
    renderer.setClearColor( 0x000000, 0 );
    renderer.setViewport( 0, 0, window.innerWidth, window.innerHeight );
    // renderer will set this eventually
    matLine.resolution.set( window.innerWidth, window.innerHeight ); // resolution of the viewport
    renderer.render( scene, camera );
    // inset scene
    renderer.setClearColor( 0x222222, 1 );
    renderer.clearDepth(); // important!
    renderer.setScissorTest( true );
    renderer.setScissor( 20, window.innerHeight - insetHeight - 20, insetWidth, insetHeight );
    renderer.setViewport( 20, window.innerHeight - insetHeight - 20, insetWidth, insetHeight );
    camera2.position.copy( camera.position );
    camera2.quaternion.copy( camera.quaternion );
    // renderer will set this eventually
    matLine.resolution.set( insetWidth, insetHeight ); // resolution of the inset viewport
    renderer.render( scene, camera2 );
    renderer.setScissorTest( false );
}
//
function initGui() {
    gui = new dat.GUI();
    var param = {
        'line type': 0,
        'width (px)': 5,
        'dashed': false,
        'dash scale': 1,
        'dash / gap': 1
    };
    gui.add( param, 'line type', { 'LineGeometry': 0, 'gl.LINE': 1 } ).onChange( function ( val ) {
        switch ( val ) {
            case '0':
                line.visible = true;
                line1.visible = false;
                break;
            case '1':
                line.visible = false;
                line1.visible = true;
                break;
        }
    } );
    gui.add( param, 'width (px)', 1, 10 ).onChange( function ( val ) {
        matLine.linewidth = val;
    } );
    gui.add( param, 'dashed' ).onChange( function ( val ) {
        matLine.dashed = val;
        // dashed is implemented as a defines -- not as a uniform. this could be changed.
        // ... or LineDashedMaterial could be implemented as a separate material
        // temporary hack - renderer should do this eventually
        if ( val ) matLine.defines.USE_DASH = ""; else delete matLine.defines.USE_DASH;
        matLine.needsUpdate = true;
        line1.material = val ? matLineDashed : matLineBasic;
    } );
    gui.add( param, 'dash scale', 0.5, 6, 0.1 ).onChange( function ( val ) {
        matLine.dashScale = val;
        matLineDashed.scale = val;
    } );
    gui.add( param, 'dash / gap', { '2 : 1': 0, '1 : 1': 1, '1 : 2': 2 } ).onChange( function ( val ) {
        switch ( val ) {
            case '0':
                matLine.dashSize = 2;
                matLine.gapSize = 1;
                matLineDashed.dashSize = 2;
                matLineDashed.gapSize = 1;
                break;
            case '1':
                matLine.dashSize = 1;
                matLine.gapSize = 1;
                matLineDashed.dashSize = 1;
                matLineDashed.gapSize = 1;
                break;
            case '2':
                matLine.dashSize = 1;
                matLine.gapSize = 2;
                matLineDashed.dashSize = 1;
                matLineDashed.gapSize = 2;
                break;
        }
    } );
}*/


/*
var camera, scene, renderer;
init();
animate();
function init() {
    camera = new THREE.PerspectiveCamera( 33, window.innerWidth / window.innerHeight, 0.1, 100 );
    camera.position.z = 10;
    scene = new THREE.Scene();
    scene.background = new THREE.Color( 0, 0, 0 );
    renderer = new THREE.SVGRenderer();
    renderer.setSize( window.innerWidth, window.innerHeight );
    document.body.appendChild( renderer.domElement );
    //
    var vertices = [];
    var divisions = 50;
    for ( var i = 0; i <= divisions; i ++ ) {
        var v = ( i / divisions ) * ( Math.PI * 2 );
        var x = Math.sin( v );
        var z = Math.cos( v );
        vertices.push( x, 0, z );
    }
    var geometry = new THREE.BufferGeometry();
    geometry.addAttribute( 'position', new THREE.Float32BufferAttribute( vertices, 3 ) );
    //
    // for ( var i = 1; i <= 3; i ++ ) {
    //     var material = new THREE.LineBasicMaterial( {
    //         color: Math.random() * 0xffffff,
    //         linewidth: 10
    //     } );
    //     var line = new THREE.Line( geometry, material );
    //     line.scale.setScalar( i / 3 );
    //     scene.add( line );
    // }
    var material = new THREE.LineDashedMaterial( {
        color: 'blue',
        linewidth: 1,
        dashSize: 10,
        gapSize: 10
    } );
    var line = new THREE.Line( geometry, material );
    line.scale.setScalar( 2 );
    scene.add( line );
    //
    window.addEventListener( 'resize', onWindowResize, false );
}
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
}
function animate() {
    var count = 0;
    var time = performance.now() / 1000;
    scene.traverse( function ( child ) {
        child.rotation.x = count + ( time / 3 );
        child.rotation.z = count + ( time / 4 );
        count ++;
    } );
    renderer.render( scene, camera );
    requestAnimationFrame( animate );
}*/


/*var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );

var renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

var geometry = new THREE.BoxGeometry( 5, 5, 1 );
var material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
var cube = new THREE.Mesh( geometry, material );
scene.add( cube );

camera.position.z = 5;

var animate = function () {
    requestAnimationFrame( animate );

    cube.rotation.x += 0.01;
    cube.rotation.y += 0.01;

    renderer.render( scene, camera );
};

animate();*//*
var renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

var camera = new THREE.PerspectiveCamera( 23, window.innerWidth / window.innerHeight, 1, 500 );
camera.position.set( 0, 0, 100 );
camera.lookAt( 0, 0, 0 );

var scene = new THREE.Scene();

var material = new THREE.LineBasicMaterial({
    color: 0x0000ff
});

var geometry = new THREE.Geometry();
geometry.vertices.push(
    new THREE.Vector3( -10, 0, 0 ),
    new THREE.Vector3( 0, 10, 0 ),
    new THREE.Vector3( 10, 0, 0 )
);

var line = new THREE.Line( geometry, material );
scene.add( line );
renderer.render( scene, camera );*/